package main

import (
	"math/rand"
	"test/testproject/src/model"
	"time"
)

func main() {

	db := model.DBConn()
	// Create Products Table
	// Migrate the schema
	db.AutoMigrate(&model.Product{})

	gender := [2]string{"Men", "Women"}
	style := [9]string{"Pain color / Red", "Pain color / Black", "Pain color / Green", "Pattern / Polka dots", "Pattern / Drawing", "Pattern / Calligraphy", "Figures / Batman", "Figures / Superman", "Figures / The flash"}
	size := [5]string{"XS", "S", "M", "L", "XL"}
	var price int
	for i, genders := range gender {
		for j, styles := range style {
			for k, sizes := range size {

				// price depends on gender style size
				price = 400 + (i*-10 + (j%3)*10 + k*10)

				db.Create(&model.Product{Gender: genders, Style: styles, Size: sizes, Price: price})
			}
		}
	}

	// Create Orders Table
	// Migrate the schema
	db.AutoMigrate(&model.Order{})

	customerId := [5]string{"001", "002", "003", "004", "005"}
	address := [5]string{"17/4 Chomthong BangKhunThian Bangkok 10150", "451 Rama I Rd Pathum Wan Bangkok 10330 ", "1692 Chatuchak Bangkok 10900", "30/39 Ngamwongwan Rd. Bangkean Nonthaburi ", "333/99 Muang Pattaya, Bang Lamung Chon Buri 20150"}
	status := [4]string{"placed order", "Paid", "Out of shipping", "completed"}

	for i, customerIds := range customerId {
		for k, status := range status {
			productId := rand.Intn(90)

			db.Create(&model.Order{ProductId: productId, CustomerId: customerIds, Quantity: k, Address: address[i], Status: status})
			if status != "placed order" {
				var orders []model.Order
				db.Last(&orders).Update("Paid_date", time.Now())
			}
		}
	}

}
