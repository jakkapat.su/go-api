package main

import (
	"encoding/json"
	"fmt"
	"log"
	"strconv"
	"test/testproject/src/model"
	"time"

	"net/http"

	"github.com/gorilla/mux"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func getProduct(w http.ResponseWriter, r *http.Request) {
	// connect db
	db := model.DBConn()

	gender := r.FormValue("Gender")
	size := r.FormValue("Size")
	style := r.FormValue("Style")

	if gender == "" {
		gender = "%%"
	}
	if size == "" {
		size = "%%"
	}
	if style == "" {
		style = "%%"
	}

	limit := r.FormValue("limit")
	rowLimit, err := strconv.Atoi(limit)

	page := r.FormValue("page")
	pageNumber, err := strconv.Atoi(page) //page number start with 0

	if err != nil {
		fmt.Fprintf(w, "err: %+v \n", err)
	}

	var products []model.Product
	result := db.Limit(rowLimit).Offset(pageNumber*rowLimit).Where("gender like  ? AND size like  ? AND style like  ?", &gender, &size, &style).Find(&products)

	if result.Error != nil {
		fmt.Fprintf(w, "result: %+v \n", result.Error)
	} else {
		fmt.Fprintf(w, "result: %+v RowsAffected \n", result.RowsAffected)
	}

	json.NewEncoder(w).Encode(products)

}

func getOrder(w http.ResponseWriter, r *http.Request) {
	// connect db
	dsn := "root:RootPassword@tcp(127.0.0.1:3306)/ics_test?charset=utf8mb4&parseTime=True&loc=Local"
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		panic("failed to connect database")
	}

	Status := r.FormValue("Status")

	if Status == "" {
		Status = "%%"
	}

	limit := r.FormValue("limit")
	rowLimit, err := strconv.Atoi(limit)

	page := r.FormValue("page")
	pageNumber, err := strconv.Atoi(page) //page number start with 0

	var orders []model.Order
	result := db.Limit(rowLimit).Offset(pageNumber*rowLimit).Where("Status like  ?  ", &Status).Find(&orders)

	if result.Error != nil {
		fmt.Fprintf(w, "result: %+v \n", result.Error)
	} else {
		fmt.Fprintf(w, "result: %+v RowsAffected \n", result.RowsAffected)
	}

	json.NewEncoder(w).Encode(orders)

}
func createOrder(w http.ResponseWriter, r *http.Request) {
	var orders model.Order
	err := json.NewDecoder(r.Body).Decode(&orders)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// connect db
	db := model.DBConn()

	db.AutoMigrate(&model.Order{})

	result := db.Create(&model.Order{ProductId: orders.ProductId, CustomerId: orders.CustomerId, Quantity: orders.Quantity, Address: orders.Address, Status: orders.Status})

	if result.Error != nil {
		fmt.Fprintf(w, "result: %+v \n", result.Error)
	} else {
		fmt.Fprintf(w, "result: %+v RowsAffected \n", result.RowsAffected)
	}
}

func updateOrder(w http.ResponseWriter, r *http.Request) {
	type updateOrder struct {
		OrderId int
		Status  string
	}

	var updateOrders updateOrder
	err := json.NewDecoder(r.Body).Decode(&updateOrders)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// connect db
	db := model.DBConn()

	var orders []model.Order
	searchResult := db.First(&orders, updateOrders.OrderId)

	if searchResult.Error != nil {
		fmt.Fprintf(w, "result: %+v \n", searchResult.Error)
	} else {

		// Save
		if updateOrders.Status != "" {

			updateResult := db.First(&orders, updateOrders.OrderId).Updates(map[string]interface{}{"Status": updateOrders.Status})

			if updateResult.Error != nil {
				fmt.Fprintf(w, "result: %+v \n", updateResult.Error)
			} else {
				// update paid date
				if updateOrders.Status == "paid" {
					db.First(&orders, updateOrders.OrderId).Update("Paid_date", time.Now())
				}
				fmt.Fprintf(w, "result: %+v RowsAffected \n", updateResult.RowsAffected)
			}
		}

	}

}

func index(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hello world")
}

func handleRequest() {
	router := mux.NewRouter()

	router.HandleFunc("/", index)
	router.HandleFunc("/getProduct", getProduct).Methods("GET")
	router.HandleFunc("/getOrder", getOrder).Methods("GET")
	router.HandleFunc("/createOrder", createOrder).Methods("POST")
	router.HandleFunc("/updateOrder", updateOrder).Methods("PATCH")

	// router port
	log.Fatal(http.ListenAndServe(":8002", router))
}
func main() {

	handleRequest()

}
