package model

import (
	"time"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

type Product struct {
	gorm.Model
	// Pid    string
	Gender string
	Style  string
	Size   string
	Price  int
}

type Order struct {
	gorm.Model
	// Oid    string
	ProductId  int
	CustomerId string
	Quantity   int
	Address    string
	Status     string
	Paid_date  time.Time
}

type updateOrder struct {
	OrderId int
	Status  string
}

// var db *gorm.DB
// var err error
func DBConn() (db *gorm.DB) {
	// Connect ot db hear
	dsn := "root:RootPassword@tcp(127.0.0.1:3306)/ics_test?charset=utf8mb4&parseTime=True&loc=Local"
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		panic("failed to connect database")
	}

	return db
}
